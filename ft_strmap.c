/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/06 14:56:14 by vde-chab          #+#    #+#             */
/*   Updated: 2014/11/08 20:32:26 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char *ft_strmap(char const *s, char (*f)(char))
{
	int		i;
	char	*map;

	if ((s == NULL && f == NULL) || (!*s && f == NULL))
		return ((char *)s);
	i = 0;
	map = ft_strdup(s);
	while (*s)
	{
		*map = f(*s);
		map++;
		s++;
		i++;
	}
	*map = '\0';
	return (map - i);
}
