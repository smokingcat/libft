Libft
=====

Libft is an all purpose Static C Library inspired from the GNU Library C (glibc).

It was my first project at 42, I use and improve it in each of my school C Projects.

Thanks to yguyot for the Readme inspiration!

###__Summary:__  

##### 1. [String manipulation](#1-string-manipulation-1)
Everything you need to manipulate strings in your program.  
__Tags :__ _[#Initialize](#initialize) [#Copy](#copy) [#Concatenate](#concatenate) [#Search](#search) [#Compare](#compare) [#Convert](#convert) [#ApplyFunction](#apply-a-function) [#Others](#others)_
##### 2. [Character manipulation](#2-character-manipulation-1)
Boolean tests and conversions for specific characters of your strings.  
__Tags :__ _[#Booltest](#is) [#ChangeCase](#change-case)_
##### 3. [Memory manipulation](#3-memory-manipulation-1)
Manipulate memory area easily.  
__Tags :__ _[#Initialize](#initialize-1) [#Move](#move) [#Analyze](#analyze)_
##### 4. [Displaying datas](#4-displaying-datas-1)
Make your program speak.  
__Tags :__ _[#Stdout](#stdout) [#FileDescriptor](#filedescriptor)_


1. String manipulation
-----------------
* * * * * * * * *
__Description :__ Everything you need to manipulate strings in your program.  
__Total :__ 29 functions  
__Tags :__ _[#Initialize](#initialize) [#Copy](#copy) [#Concatenate](#concatenate) [#Search](#search) [#Compare](#compare) [#Convert](#convert) [#ApplyFunction](#apply-a-function) [#Others](#others)_  
* * * * * * * * *

* ##### Initialize
    * char        __*ft_strnew(size_t size)__;  
    _↳ Creates and initializes a new string._

    * void         __ft_strdel(char **as)__;  
    _↳ Frees the string pointed by as and reinitializes the pointer._

    * void         __ft_strclr(char *s)__;  
    _↳ Clears the string pointed by s._

* ##### Copy
    * char        __*ft_strcpy(char *s1, const char *s2)__;  
    _↳ Copies string s2 in string s1._

    * char        __*ft_strncpy(char *s1, const char *s2, size_t n)__;  
    _↳ Copies n characters from string s2 in string s1._

* ##### Concatenate
    * char        __*ft_strcat(char *s1, const char *s2)__;  
    _↳ Appends a copy of string s2 to the end of string s1. The string s1 must have sufficient space to hold the result._

    * char        __*ft_strncat(char *s1, const char *s2, size_t n)__;  
    _↳ Appends a copy of not more than n characters from string s2 to the end of string s1. The string s1 must have sufficient space to hold the result._

    * size_t        __*ft_strlcat(char *dst, const char *src, size_t size)__;  
    _↳ The strlcat() function appends the string src to the end of dst.  It will append at most size - strlen(dst) - 1 bytes, NUL-terminating the result._

* ##### Search

    * char        __*ft_strchr(const char *s, int c)__;  
    _↳ Locates the first occurence of c in the string pointed to by s. Return a pointer to the located character, or NULL if the character does not appear in the string._

    * char        __*ft_strrchr(const char *s, int c)__;  
    _↳ Identical to strchr(), except it locates the last occurence of c._

    * char        __*ft_strstr(const char *s1, const char *s2)__;  
    _↳ Locates the first occurence of the string s2 in the string s1._

    * char        __*ft_strnstr(const char *s1, const char *s2, size_t n)__;  
    _↳ Locates the first occurence of the string s2 in the string s1, where no more than n characters are searched._

* ##### Compare

    * int            __*ft_strcmp(const char *s1, const char *s2)__;  
    _↳ Lexicographically compares the strings s1 and s2 and return the difference._

    * int            __*ft_strncmp(const char *s1, const char *s2, size_t n)__;  
    _↳ Lexicographically compares not more than n characters in the strings s1 and s2 and return the difference._

    * int           __ft_strequ(char const *s1, char const *s2)__;  
    _↳ Lexicographically compares the strings s1 and s2 and return 1 if they are equal or 0._

    * int           __ft_strnequ(char const *s1, char const *s2, size_t n)__;  
    _↳ Lexicographically compares not more than n characters the strings s1 and s2 and return 1 if they are equal or 0._

* ##### Convert

    * int            __*ft_atoi(const char *str)__;  
    _↳ Converts the string pointed to by str to int representation._

    * char           __*ft_itoa(int n)__;  
    _↳ Converts the integer to new string._

* ##### Apply a function

    * void          __ft_striter(char *s, void (*f)(char *))__;  
    _↳ Applies function f on each character of the string pointed by s._

    * void          __ft_striteri(char *s, void (*f)(unsigned int, char *))__;  
    _↳ Applies function f on each character of the string pointed by s._

    * char          __*ft_strmap(char const *s, char (*f)(char))__;  
    _↳ Applies function f on each character of the string pointed by s and return the result in a new string._

    * char          __*ft_strmapi(char const *s, char (*f)(unsigned int, char))__;  
    _↳ Applies function f on each character of the string pointed by s and return the result in a new string._

* ##### Others

    * size_t        __*ft_strlen(const char *s)__;  
    _↳ Computes the length of the string s._

    * char         __*ft_strdup(const char *s1)__;  
    _↳ Duplicates string s1 and returns a pointer to the new string._

    * char           __*ft_strsub(char const *s, unsigned int start, size_t len)__;  
    _↳ Truncates the string pointed by s in a new string._

    * char           __*ft_strjoin(char const *s1, char const *s2)__;  
    _↳ Joins two strings in a new string._

    * char           __*ft_strtrim(char const *s)__;  
    _↳ Returns a new version of the string pointed by s without ' ', '\n' and '\t'._

    * char           __**ft_strsplit(char const *s, char c)__;  
    _↳ Cuts the string pointed by s at each occurence of character c._

    * int             __ft_countc(char *str, char c, size_t length)__;  
    _↳ Counts each occurence of character 'c' in the 'length' first characters of 'str'._

2. Character manipulation
-----------------
* * * * * * * * *
__Description :__ Boolean tests and conversions for specific characters of your strings.  
__Total :__ 7 functions  
__Tags :__ _[#Booltest](#is) [#ChangeCase](#change-case)_  
* * * * * * * * *

* ##### Booltest

    * int            __ft_isalpha(int c)__;  
    _↳ Returns 0 if the character test false or 1._

    * int            __ft_isdigit(int c)__;  
    _↳ Returns 0 if the character test false or 1._

    * int            __ft_isalnum(int c)__;  
    _↳ Returns 0 if the character test false or 1._

    * int            __ft_isascii(int c)__;  
    _↳ Returns 0 if the character test false or 1._

    * int            __ft_isprint(int c)__;  
    _↳ Returns 0 if the character test false or 1._

* ##### Change case

    * int            __ft_toupper(int c)__;  
    _↳ Converts a lower-case letter to the corresponding upper-case letter._

    * int            __ft_tolower(int c)__;  
    _↳ Converts a upper-case letter to the corresponding lower-case letter._

3. Memory manipulation
-----------------
* * * * * * * * *
__Description :__ Manipulate memory area easily.  
__Total :__ 12 functions  
__Tags :__ _[#Initialize](#initialize-1) [#Move](#move) [#Analyze](#analyze)_  
* * * * * * * * *

* ##### Initialize

    * void         __*ft_memalloc(size_t size)__;  
    _↳ Allocates and initializes a fresh memory area and returns it._

    * void         __ft_memdel(void **ap)__;  
    _↳ Frees the memory area pointed by the value pointed by ap and initializes this pointer._

    * void         __*ft_realloc(void *mem, size_t size, size_t newsize)__;  
    _↳ Save the memory area pointed by mem in a new memory area of 'new size' size, then it frees memory area pointed by mem before returns this new memory area._

    * void         __*ft_memset(void *b, int c, size_t len)__;  
    _↳ Writes 'len' bytes of value 'c' (converted to an unsigned char) to the byte string 'b' and returns it._

    * void         __ft_bzero(void *s, size_t n)__;  
    _↳ Writes 'n' zeroed bytes to the string 's'._

* ##### Move

    * void         __*ft_memcpy(void *s1, const void *s2, size_t n)__;  
    _↳ Copies n bytes from memory area s2 to memory area s1. If s1 and s2 overlap, behavior is undefined._

    * void         __*ft_memccpy(void *s1, const void *s2, int c, size_t n)__;  
    _↳ Copies bytes from string s2 to string s1.  If the character c (as converted to an unsigned char) occurs in the string s2, the copy stops and a pointer to the byte after the copy of c in the string s1 is returned. Otherwise, n bytes are copied, and a NULL pointer is returned. The source and destination strings should not overlap, as the behavior is undefined.._

    * void         __*ft_memmove(void *s1, const void *s2, size_t n)__;  
    _↳ Copies n bytes from string s2 to string s1. The two strings may overlap; the copy is always done in a non-destructive manner._

    * void         __ft_swap(void *n1, void *n2)__;  
    _↳ Inverts 'n1' and 'n2' values._

    * void         __ft_revert(void *tab, size_t n)__;  
    _↳ Reverse the memory area pointed by tab on 'n' bytes._

* ##### Analyze

    * void         __*ft_memchr(const void *s, int c, size_t n)__;  
    _↳ Locates the first occurence of 'c' (converted to an unsigned char) in string 's'._

    * int          __ft_memcmp(const void *s1, const void *s2, size_t n)__;  
    _↳ Compares byte string s1 against byte string s2.  Both strings are assumed to be n bytes long. It returns the difference between the first two differing bytes or 0 if the two strings are identical._

4. Displaying datas
-----------------
* * * * * * * * *
__Description :__ Make your program speak.  
__Total :__ 8 functions  
__Tags :__ _[#Stdout](#stdout) [#FileDescriptor](#filedescriptor)_  
* * * * * * * * *

* ##### Stdout

    * void         __ft_putchar(char c)__;  
    _↳ Print character 'c' on stdout._

    * void         __ft_putstr(char const *s)__;  
    _↳ Print string 's' on stdout._

    * void         __ft_putendl(char const *s)__;  
    _↳ Print string 's' and then character '\n' on stdout._

    * void         __ft_putnbr(int n)__;  
    _↳ Print number 'n' on stdout._

* ##### FileDescriptor

   * void         __ft_putchar_fd(char c, int fd)__;  
    _↳ Print character 'c' on file descriptor 'fd'._

    * void         __ft_putstr_fd(char const *s, int fd)__;  
    _↳ Print string 's' on file descriptor 'fd'._

    * void         __ft_putendl_fd(char const *s, int fd)__;  
    _↳ Print string 's' and then character '\n' on file descriptor 'fd'._

    * void         __ft_putnbr_fd(int n, int fd)__;  
    _↳ Print number 'n' on file descriptor 'fd'._